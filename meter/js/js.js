async function sendForm() {
  const url = "https://api.www.otevrenamesta.cz/items/simpleware/";
  const data = {
    contact: document.getElementById("contact").value,
    name: document.getElementById("name").value,
  };
  try {
    const res = await fetch(url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(data),
    });
    alert("Děkujeme, ozveme se.");
  } catch (err) {
    alert(`vyskytla se chyba.
    ${err.toString()}`);
  }
}
